package com.adamkorzeniak.utils.configuration;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.util.Objects;
import java.util.Properties;

public class YamlPropertySourceFactory implements PropertySourceFactory {

    private static final String PROPERTY_SOURCE_NAME_NOT_EMPTY_MESSAGE
            = "Property source name must contain at least one character";
    private static final String PROPERTY_SOURCE_NOT_NULL_MESSAGE
            = "Property source must not be null";

    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource encodedResource) {
        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
        Resource resource = encodedResource.getResource();
        factory.setResources(resource);
        Properties properties = factory.getObject();

        Objects.requireNonNull(resource.getFilename(), PROPERTY_SOURCE_NAME_NOT_EMPTY_MESSAGE);
        Objects.requireNonNull(properties, PROPERTY_SOURCE_NOT_NULL_MESSAGE);

        return new PropertiesPropertySource(resource.getFilename(), properties);
    }
}
